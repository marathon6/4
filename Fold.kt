// Return the set of products that were ordered by all customers
fun Shop.getProductsOrderedByAll(): Set<Product> {
    val orederedProducts = customers.flatMap{it.getOrderedProducts()}.toSet()
    return (
            customers.fold(orederedProducts){
                    orderedByAll, customer ->
                orderedByAll.intersect(customer.getOrderedProducts())
            }
            )
}

fun Customer.getOrderedProducts(): List<Product> =
    orders.flatMap (Order::products)